import React from 'react';

function Reference() {
  return (
    <section id="reference" className="section reference">
        reference
        <p>Here you can see some projects i did for clients in my free-time:</p>
        <a rel="noopener noreferrer" href="https://eberhard-seminare.ch/" target="_blank">
          <div className="reference-preview">
            <img src="eberhard-seminare.png" alt="Eberhard Seminare Preview"></img>
            <div>
              <div>Eberhard Seminare</div>
            </div>
          </div>
        </a>
        <a rel="noopener noreferrer" href="https://benjarong.ch/" target="_blank">
          <div className="reference-preview">
            <img src="benjarong.png" alt="Benjarong Preview"></img>
            <div>
              <div>Benjarong</div>
            </div>
          </div>
        </a>
    </section>
  );
}

export default Reference;