import React from 'react';

function LightSwitch(props) {
  return (
    <div className="sticky lightswitch">
      <img onClick={() => props.setLight(false)} id="moon" src={props.isBackgroundlight === false ? "moon-filled.svg" : "moon.svg"} alt="moon" width="35px" height="35px"/>
      <img onClick={() => props.setLight(true)} id="sun" src={props.isBackgroundlight === true ? "sun-filled.svg" : "sun.svg"} alt="sun" width="35px" height="35px"/>
    </div>
  );
}

export default LightSwitch;