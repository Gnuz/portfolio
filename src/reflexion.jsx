import React from 'react';

function Reflexion() {
  return (
    <div className="reflexion">
      <h1>Reflexion</h1>
        <h2>Verschiedene Arten des Urheberrechts (K2)</h2>
          <h3>Copyright</h3>
            <p>Der Ersteller hat das Recht zu entscheiden wer und unter welchen Umständen sein Werk benutzt werden kann.</p>
          <h3>Fair use</h3>
            <p>Das Werk darf nicht kommerziel, also für den Gewinn von Geld, benutzt werden.</p>
          <h3>Public Domain</h3>
            <p>Man darf mit dem Werk machen was man will.</p>
          <h3>Creative Commons</h3>
            <p>Ist unterteilt in 6 verschiedene Arten. Je nach Art muss man den Autor verlinken, es nicht bearbeiten, nicht für kommerziele Zwecke benutzt werden oder verschieden Kombinationen daraus.</p>
        
        <h2>Dateiformate vergleichen (K4)</h2>
          <h3>Video-Formate</h3>
            <h4>mp4</h4>
              <ul>
                <li><p>+ hohe qualität</p></li>
                <li><p>+ geringer Speicherplatz</p></li>
                <li><p>- leistungsintensiv</p></li>
              </ul>
            <h4>mkv</h4>
              <ul>
                <li><p>+ gute qualität</p></li>
                <li><p>+ gute Komporessionsmöglighkeiten</p></li>
                <li><p>- nicht auf allen geräten unterstützt</p></li>
              </ul>
            <h4>WebM</h4>
              <ul>
                <li><p>+ gute qualität</p></li>
                <li><p>+ geringer Speicherplatz</p></li>
                <li><p>- nicht auf allen geräten unterstützt</p></li>
              </ul>
            <h4>mov</h4>
              <ul>
                <li><p>+ gut für Videoschnitt</p></li>
                <li><p>- nicht auf allen geräten unterstützt</p></li>
              </ul>
            <p>Für Web ist mp4 und WebM am besten geeignet, hauptsächlich wegen dem geringen Speicherplatz und der grossen Kompatibiltät. 
              WebM ist jedoch eher neu und deshalb noch nicht ganz so vertreten, es wird aber oft als die Zukunft beschrieben. 
              Mov kommt von QuickTime und wird in Videoschnittprogrammen gerne verwendet. 
              MKV wird für Heimkino verwendet, da man viele Zusatzdaten hinzufügen kann wie zum Beispiel Untertitel.</p>

          <h3>Bild-Formate</h3>
            <h4>jpg</h4>
              <img src="face.jpg" alt="Gesicht im JPG Format"/>
              <ul>
                <li><p>+ geringer Speicherplatz</p></li>
                <li><p>- keine Transparenz</p></li>
              </ul>
            <h4>png</h4>
              <img src="face.png" alt="Gesicht im PNG Format"/>
              <ul>
                <li><p>+ Verlustfrei komprimirung</p></li>
                <li><p>+ unterstützt Transparenz</p></li>
                <li><p>- grosse Dateien</p></li>
              </ul>
            <h4>raw</h4>
              <ul>
                <li><p>Meine Webcam erstellt leider keine raws, deshalb habe ich auch kein Beispiel.</p></li>
                <li><p>+ 68 Milliared Farben</p></li>
                <li><p>- sehr grosse Dateien</p></li>
              </ul>
            <p>Bei einem Bild ohne Transparenz eignet sich ein jpg am besten und wird auch eingesetzt. Bei einem Bild mit Transparenz muss man ein png benutzten. Raws sind wie der Name sagt rohe Bilder, heisst sehr hohe Auflösung und viele Farben.</p>
              
          <h3>Audio-Formate</h3>
            <h4>mp3</h4>
            <h4>
              <audio controls>
                <source src="hallo.mp3" type="audio/mpeg" />
                Dein Browser unterstützt kein mp3 Format.
              </audio>
            </h4>
              <ul>
                <li><p>+ sehr bekannt</p></li>
                <li><p>+ gute Komporessionsmöglighkeiten</p></li>
                <li><p>- Qualitätsverlust möglich</p></li>
              </ul>
            <h4>wav</h4>
            <h4>
              <audio controls>
                <source src="hallo.wav" type="audio/wav" />
                Dein Browser unterstützt kein wav Format.
              </audio>
            </h4>
              <ul>
                <li><p>+ gute qualität</p></li>
                <li><p>- sehr grosse Dateien</p></li>
              </ul>
            <h4>aac</h4>
            <h4>
              <audio controls>
                <source src="hallo.aac" type="audio/mp4" />
                Dein Browser unterstützt kein aac Format.
              </audio>
            </h4>
              <ul>
                <li><p>+ geringer Speicherplatz</p></li>
                <li><p>+ gute qualität</p></li>
                <li><p>- nicht auf allen geräten unterstützt</p></li>
              </ul>
            <p>Mp3 ist sehr etabliert und lässt sich überall finden. Aac wird als nachgolger von mp3 gesehen und ist dehalb in qualität und grösse besser, jedoch noch nicht so verbreitet. Wav ist ähnlich wie raw bei den Bildern, sprich komplett unkomprimiert.</p>

            <h4>Die genauen und aktuellen Kompatibiltäten können zum Beispiel hier überprüft werden: <a href="https://caniuse.com/" target="_blank" rel="noopener noreferrer">CanIUse</a></h4>

        <h2>Animationen (K5)</h2>
          <p>
            Der Referenz-Bilder hover effect und die Scroll slide in sind meine Animationen. 
            Das erste ist mein eigenes CSS und beim slide in benutze ich eine React library 
            namens: <a rel="noopener noreferrer" href="https://www.npmjs.com/package/react-animate-on-scroll">react-animate-on-scroll</a>. Diese benutzt <a rel="noopener noreferrer" href="https://daneden.github.io/animate.css/">animate.css</a> für die Animationen.
            Beides war neu für mich aber sehr einfach zu benutzen.
          </p>

        <h2>Automatisierung (K6)</h2>
          <p>
            Die Webseite hat einen light und dark mode. Dieser ist von mir selbst gemacht. 
            Die Standardeinstellung beim Besuch der Webseite ist je nach Uhrzeit anderst, ab 18 Uhr wird der dark mode ausgewählt, davor der light mode. 
          </p>

        <h2>Testing (K7)</h2>
            <h4>Funktionieren die geplanten Animationen</h4>
            <p>Ja, beide Animationen die ich mir vorgenommen hatte funktionieren einwandfrei.</p>

            <h4>Ist die Webseite abrufbar</h4>
            <p>Ja, <a href="https://pedantic-mahavira-5ec109.netlify.com/" target="_blank" rel="noopener noreferrer">hier</a></p>

            <h4>Funktionieren alle Links auf der Seite</h4>
            <p>Ja.</p>

            <h4>Sind alle benötigten Informationen erhältlich</h4>
            <p>Ja.</p>

            <h4>Gibt es keine Fehlermeldungen in der Konsole</h4>
            <p>Nein.</p>

          <h3>Browserkompatibilität</h3>
            <p>Firefox und Chrome haben schon von anfang an hut funktioniert, für den Edge/IE musste ich noch eine Code Anpassung mache.</p>
            <img width="30%" src="chrome-test.png" alt="Chrome Test"/>
            <img width="30%" src="firefox-test.png" alt="firefox Test"/>
            <img width="30%" src="edge-test.png" alt="edge Test"/>

          <h2>Reflexion (K8)</h2>
            <p>
              Ich habe über die ganze Erstellung dieser Seite gelernt. Einerseits habe ich den ganzen Prozess von Idee, Design und Verwirklichung viel mehr verinnerlicht.
              Andernseits kenne ich mich auch in React und CSS noch mehr aus. In React habe ich einiges drüber gelernt mit Funktionalen Komponenten umzugehen, bis jetzt hatte ich mehr erfahrung mit Klassen.
              In CSS habe ich mich noch mehr in Selectoren eingearbeitet, zum Beispiel diesen ".background-light > * a". Er spricht alle indirekten Children der Klasse background-light des tags "a" an.
            </p>
            <p>
              Alles in allem hatte ich sehr viel Spass beim Modul und war froh mal ein Modul mit mehr kreativem Auslauf zu haben. Mit dem Endprodukt bin ich sehr zufrieden und stolz.
            </p>
            
          <h2>Responsive (K10)</h2>
            <p>
              Die Hauptseite ist Responsive, die Reflexions-Seite nicht komplett.  
            </p>
   </div>
  );
}

export default Reflexion;