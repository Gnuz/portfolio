import React from 'react';
import AnchorLink from 'react-anchor-link-smooth-scroll';

function Nav() {
  return (
    <nav className="sticky nav">
      <AnchorLink href="#contact">contact</AnchorLink>
      <AnchorLink href="#reference">reference</AnchorLink>
      <AnchorLink href="#about">about</AnchorLink>
    </nav>
  );
}

export default Nav;