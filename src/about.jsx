import React from 'react';

function About() {
  return (
    <section id="about" className="section about">
        Hi there!
        I'm Lino Iten, a computer science student in a apprentisship at "Siemens Schweiz AG". This here acts as a early stage of my own personal landing page, mostly about my web-development skills. If you're interested to work with me please contact me below.
    </section>
  );
}

export default About;