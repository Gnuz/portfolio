import React from 'react';
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <section className="footer">
      <div className="copyright">
        <span>
          Copyright © 2020 by Lino Iten
          <br />
          All rights reserved. <Link to="/reflexion">mehr Info's in der Reflexion</Link>
        </span>
      </div>
    </section>
  );
}

export default Footer;