import React from 'react';
import Typical from 'react-typical' 

class Home extends React.PureComponent {
  render() {
    return (
      <section className="section home">
        <Typical
          steps={['Web Developer', 1000, 'Web Developer based in Switzerland', 1000, 'Web Developer based in Switzerland now available for you!']}
          loop={1}
          wrapper="p"
        />
      </section>
    );
  }
}

export default Home;