import React, { useState, useEffect } from 'react';
import './style/App.css';
import { Route } from "react-router-dom";

import LightSwitch from './lightSwitch';
import Nav from './nav';
import Home from './home';
import About from './about';
import Reference from './reference';
import Contact from './contact';
import Footer from './footer';
import Reflexion from './reflexion';

import UIfx from 'uifx';
import ScrollAnimation from 'react-animate-on-scroll';
import "animate.css/animate.min.css";

import halloAudio from './sounds/hallo.mp3';

const hallo = new UIfx(
  halloAudio,
  {
    volume: 1,
    throttleMs: 100
  }
)

function App() {
  const [isBackgroundlight, setIsBackgroundlight] = useState(false);

  const setLight = (value) => {
    setIsBackgroundlight(value);
  }

  useEffect(() => {
    hallo.play()
    const date = new Date();
    if (date.getHours() < 18) {
      setLight(true)
    }
  }, []);

  return (
    <div className={isBackgroundlight ? 'background-light root' : 'background-dark root'}>
      <LightSwitch setLight={setLight} isBackgroundlight={isBackgroundlight} />
      <Route path="/" exact>
          <Nav />
          <Home />
          <ScrollAnimation animateIn="slideInLeft">
            <About />
          </ScrollAnimation>
          <ScrollAnimation animateIn="slideInLeft">
            <Reference />
          </ScrollAnimation>
          <ScrollAnimation animateIn="slideInLeft">
            <Contact />
          </ScrollAnimation>
          <Footer />
      </Route>
      <Route path="/reflexion">
        <Reflexion />
      </Route>
    </div>
  );
}

export default App;
