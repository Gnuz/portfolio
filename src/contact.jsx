import React from 'react';

function Contact() {
  return (
    <section id="contact" className="section contact">
      contact
      <p>The best way to reach me is via my email: <a href="mailto:lino.iten@hotmail.com?subject=lets work together">lino.iten@hotmail.com</a></p>
    </section>
  );
}

export default Contact;